import unfetch from 'unfetch'

export default class CallbackButton {
  static load(a,b,c) {
    // debugger
    a()
  }
  validateForm() {
    const{form}=this
    const isValid = form.reportValidity()
    if (!isValid) return
    const body = new FormData(form)
    return body
    // const data= {
    //   name: /** @type {string} */ (body.get('name')),
    //   phone: /** @type {string} */ (body.get('phone')),
    // }
    // var formData = new FormData()
    // for (const k in data) {
    //   const v=data[k]
    //   formData.append(k,v)
    // }
    // return {formData,data}
    // formData.append('x')
  }
  hideFormError() {
    // this.FormError.style.display='none !important'
    this.FormError.style.setProperty('display','none', 'important')
  }
  showFormError(error) {
    // debugger
    // console.log(error,'err')
    this.FormError.style.display=''
  }
  hideFormSuccess() {
    this.FormSuccess.style.setProperty('display','none', 'important')
    // this.FormSuccess.style.display='none !important'
  }
  showFormSuccess(name='') {
    this.FormSuccess.style.display=''
    this.FormName.innerHTML=name.split(/\s/)[0]
    this.el.style.setProperty('display', 'none', 'important')
  }
  addCompleted() {
    this.form.classList.add('ContactFormCompleted')
  }
  removeCompleted() {
    this.form.classList.remove('ContactFormCompleted')
  }
  disableInputs(disable=true) {
    disable=!!disable
    const INPUTS=new Set(['name','phone'])
    for (const input of this.form.querySelectorAll('input')) {
      const NAME=input.getAttribute('name')
      if(INPUTS.has(NAME)) {
        input.disabled=disable
      }
    }
  }
  constructor(el){
    /** @type {HTMLButtonElement} */
    this.el=el
    const form=el.closest('form')
    /** @type {HTMLFormElement} */
    this.form=form

    /** @type {HTMLParagraphElement} */
    this.FormError=/** @type {HTMLParagraphElement} */(this.form.querySelector('#FormError'))
    /** @type {HTMLParagraphElement} */
    this.FormSuccess=/** @type {HTMLParagraphElement} */(this.form.querySelector('#FormSuccess'))
    /** @type {HTMLSpanElement} */
    this.FormName=/** @type {HTMLSpanElement} */(this.form.querySelector('#FormName'))

    /** @type {HTMLAnchorElement} */
    this.CloseFormErrorNotice=/** @type {HTMLAnchorElement} */(this.form.querySelector('#CloseFormErrorNotice'))

    this.CloseFormErrorNotice.addEventListener('click',(e) => {
      e.preventDefault()
      this.hideFormError()
      return false
    })

    /** @type {EventListener} */
    this.submitListener=(e)=>{
      // load data from server
      // const{form}=this
      const formData=this.validateForm()
      e.preventDefault()
      // debugger
      const{action}=this.form
      if(formData&&action) {
        this.hideFormError()
        formData.set('message',true)
        return unfetch(action,{
          method:'POST',
          body:formData,
        }).then((res)=>{
          // debugger
          // console.log('data fetched')
          return res.json().then((json)=>{
            const{'success':success}=json
            if(!success) {
              this.showFormError(':(')
            } else {
              // this.showFormError(err)
              this.addCompleted()
              this.disableInputs()
              this.showFormSuccess(formData.get('name'))
            }
            // set that message is sent.
          })
        },(err) =>{
          this.showFormError(err)
        })
        // fetch ajax
      }

      // console.log('data')
      // debugger
      return false
    }
  }
  reportError() {
    // set the error
  }
  reportSuccess() {
    // set the success
  }
  render() {
    const{form,submitListener}=this
    form.addEventListener('submit',submitListener)
  }
  unrender() {
    const{form,submitListener}=this
    form.removeEventListener('submit',submitListener)
  }
}

CallbackButton['plain'] = true