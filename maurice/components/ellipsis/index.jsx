import { Component } from 'preact'
import { EllipsisSpan } from './lib'

/**
 * A _Preact_ component that displays loading ... dots animation.
 * It has a `serverRender` method that calls `splendid.export` to
 * make sure this component is present on pages.
 * The other way would be to create an `elements/ellipsis` element
 * and call this function there (elements override components but
 * only for server-side rendering).
 * @example
 */
export default class Ellipsis extends Component {
  constructor() {
    super()
    this.state.showing = false
  }
  componentDidMount() {
    this.setState({ showing: true })
  }
  /**
   * Called by _Splendid_ when component is scrolled out of view.
   */
  componentWillUnmount() {
    this.setState({ showing: false })
  }
  render({ timeout = 250, children }, { showing }) {
    return (<EllipsisSpan showing={showing} timeout={timeout}>{children}</EllipsisSpan>)
  }
}
