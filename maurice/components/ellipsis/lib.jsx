import { Component } from 'preact'

export const EllipsisSpan = ({ id, showing, children, timeout }) => {
  return (<span id={id}>
    {children}
    {showing && <Dots timeout={timeout}/>}
  </span>)
}

export class Dots extends Component {
  constructor() {
    super()
    this.state = {
      inc: 0,
    }
    this.int = null
  }
  componentDidMount() {
    this.int = setInterval(() => {
      let inc = this.state.inc + 1
      if (inc > 3) inc = 0
      this.setState({ inc })
    }, this.props.timeout)
  }
  /**
   * This function is called by _Preact_, because the root
   * component changed its state and removed the Dots element.
   */
  componentWillUnmount() {
    clearInterval(this.int)
  }
  render() {
    return (<span>{'.'.repeat(this.state.inc)}</span>)
  }
}
