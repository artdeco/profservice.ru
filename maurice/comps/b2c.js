import makeClassGetter from './__mcg'
const renameMaps = {  }
import { makeIo, init, startPlain } from './__competent-lib'
import CallbackButton from '../components/callback-button/index.js'
import Parallax from 'splendid/build/components/parallax/index'

const __components = {
  'callback-button': () => CallbackButton,
  'parallax': () => Parallax,
}

const io = makeIo()

const meta = [{
  key: 'parallax',
  id: 'c2c4d',
  props: {
    'translate-x': '-430',
    'acc-y': '-1',
    'translate-y': 100,
    'rel-image': 'blocks/footer/deco/squares1.png',
    once: true,
  },
},
{
  key: 'parallax',
  id: 'cbadf',
  props: {
    'translate-x': 500,
    'acc-y': '.5',
    'translate-y': 0,
    'rel-image': 'blocks/order-now/deco/squares3.svg',
    once: true,
  },
},
{
  key: 'parallax',
  id: 'cd60f',
  props: {
    'translate-x': 0,
    'acc-y': 1,
    'translate-y': '-600',
    'rel-image': 'blocks/top-parallax/deco/squares-r.svg',
    once: true,
  },
},
{
  key: 'parallax',
  id: 'cb982',
  props: {
    'translate-x': 100,
    'acc-y': '-.5',
    'translate-y': 0,
    'rel-image': 'blocks/numbers-parallax/deco/squares5.svg',
    once: true,
  },
},
{
  key: 'parallax',
  id: 'c0461',
  props: {
    'translate-x': 100,
    'acc-y': '.5',
    'translate-y': 0,
    'rel-image': 'blocks/numbers-parallax/deco/squares8.png',
    once: true,
  },
},
{
  key: 'callback-button',
  id: 'c486d',
  children: ["\n                Оставить заявку\n              "],
},
{
  key: 'parallax',
  id: 'ccd41',
  props: {
    'translate-x': '-175',
    'acc-y': 1,
    'translate-y': 0,
    'rel-image': 'blocks/footer/deco/squares1.png',
    once: true,
  },
},
{
  key: 'parallax',
  id: 'ca232',
  props: {
    'translate-x': 135,
    'acc-y': '.5',
    'translate-y': 327,
    'rel-image': 'blocks/top-parallax/deco/square2.svg',
    once: true,
  },
},
{
  key: 'parallax',
  id: 'c1d16',
  props: {
    'translate-x': 0,
    'acc-y': '-1',
    'translate-y': '-255',
    'rel-image': 'blocks/order-now/deco/squares3.svg',
    once: true,
  },
},
{
  key: 'parallax',
  id: 'c59ed',
  props: {
    'translate-x': '-130',
    'acc-y': 1,
    'translate-y': '-400',
    'rel-image': 'blocks/stages-parallax/deco/squares7.png',
    once: true,
  },
},
{
  key: 'parallax',
  id: 'c302d',
  props: {
    'translate-x': '-50',
    'acc-y': 1,
    'translate-y': 0,
    'rel-image': 'blocks/our-work/deco/squares.png',
    once: true,
  },
},
{
  key: 'parallax',
  id: 'c95a6',
  props: {
    'translate-x': 60,
    'acc-y': 1,
    'translate-y': 400,
    'rel-image': 'blocks/stages-parallax/deco/squares6.png',
    once: true,
  },
},
{
  key: 'parallax',
  id: 'c7036',
  props: {
    'translate-x': 0,
    'acc-y': '-1',
    'translate-y': 100,
    'rel-image': 'blocks/order-now/deco/squares-r2.svg',
    once: true,
  },
},
{
  key: 'parallax',
  id: 'c94d3',
  props: {
    'translate-x': 50,
    'acc-y': 2,
    'translate-y': 200,
    'rel-image': 'blocks/our-work/deco/squares2.png',
    once: true,
  },
},
{
  key: 'parallax',
  id: 'c0f4c',
  props: {
    'translate-x': '-90',
    'acc-y': '-1.5',
    'translate-y': '-225',
    'rel-image': 'blocks/top-parallax/deco/squares1.svg',
    once: true,
  },
}]
meta.forEach(({ key, id, props = {}, children = [] }) => {
  const Comp = __components[key]()
  const plain = true
  props.splendid = { mount: '/', addCSS(stylesheet) {
    return makeClassGetter(renameMaps[stylesheet])
  }, css(paths, rootSelector, { mapName } = {}) {
    if (mapName && !mapName.endsWith('.js')) {
      mapName = mapName += '.js'
    }
    return this.addCSS(mapName || paths)
  },
  elementRelative(path) { return `${path}` },
  export() {},
}

  const ids = id.split(',')
  ids.forEach((Id) => {
    const { parent, el } = init(Id, key)
    if (!el) return
    const renderMeta = /** @type {_competent.RenderMeta} */ ({ key, id: Id, plain })
    let comp
    el.rerender = () => {
      if (!comp) return
      const getComp = () => __components[key]()
      comp.rerender(getComp, { children, ...props })
    }
    el.render = () => {
      comp = startPlain(renderMeta, Comp, comp, el, parent, props, children)
      return comp
    }
    el.render.meta = renderMeta
    io.observe(el)
  })
})
