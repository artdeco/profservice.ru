export default () => {
  /* eslint-env browser */
  const ents = [...document.querySelectorAll('div[data-section]')]

  if (ents.length) {
    const io = new IntersectionObserver((entries) => {
      entries.forEach(({ target, isIntersecting }) => {
        const section = target.id

        if (isIntersecting) {
          const li = document.querySelector(`[data-heading="${section}"]`)
          li.classList.add('Active')
        } else {
          const li = document.querySelector(`[data-heading="${section}"]`)
          li.classList.remove('Active')
        }
      })
    }, {  })

    ents.forEach((el) => {
      io.observe(el)
    })
  }
}