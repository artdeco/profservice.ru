import makeClassGetter from './__mcg'
const renameMaps = {  }
import { makeIo, init, startPlain } from './__competent-lib'
import CallbackButton from '../components/callback-button/index.js'
import Parallax from 'splendid/build/components/parallax/index'

const __components = {
  'callback-button': () => CallbackButton,
  'parallax': () => Parallax,
}

const io = makeIo()

const meta = [{
  key: 'parallax',
  id: 'c2c4d',
  props: {
    'translate-x': '-430',
    'acc-y': '-1',
    'translate-y': 100,
    'rel-image': 'blocks/footer/deco/squares1.png',
    once: true,
  },
},
{
  key: 'parallax',
  id: 'cd60f',
  props: {
    'translate-x': 0,
    'acc-y': 1,
    'translate-y': '-600',
    'rel-image': 'blocks/top-parallax/deco/squares-r.svg',
    once: true,
  },
},
{
  key: 'callback-button',
  id: 'c486d',
  children: ["\n                Оставить заявку\n              "],
},
{
  key: 'parallax',
  id: 'ccd41',
  props: {
    'translate-x': '-175',
    'acc-y': 1,
    'translate-y': 0,
    'rel-image': 'blocks/footer/deco/squares1.png',
    once: true,
  },
},
{
  key: 'parallax',
  id: 'ca232',
  props: {
    'translate-x': 135,
    'acc-y': '.5',
    'translate-y': 327,
    'rel-image': 'blocks/top-parallax/deco/square2.svg',
    once: true,
  },
},
{
  key: 'parallax',
  id: 'c0f4c',
  props: {
    'translate-x': '-90',
    'acc-y': '-1.5',
    'translate-y': '-225',
    'rel-image': 'blocks/top-parallax/deco/squares1.svg',
    once: true,
  },
}]
meta.forEach(({ key, id, props = {}, children = [] }) => {
  const Comp = __components[key]()
  const plain = true
  props.splendid = { mount: '/', addCSS(stylesheet) {
    return makeClassGetter(renameMaps[stylesheet])
  }, css(paths, rootSelector, { mapName } = {}) {
    if (mapName && !mapName.endsWith('.js')) {
      mapName = mapName += '.js'
    }
    return this.addCSS(mapName || paths)
  },
  elementRelative(path) { return `${path}` },
  export() {},
}

  const ids = id.split(',')
  ids.forEach((Id) => {
    const { parent, el } = init(Id, key)
    if (!el) return
    const renderMeta = /** @type {_competent.RenderMeta} */ ({ key, id: Id, plain })
    let comp
    el.rerender = () => {
      if (!comp) return
      const getComp = () => __components[key]()
      comp.rerender(getComp, { children, ...props })
    }
    el.render = () => {
      comp = startPlain(renderMeta, Comp, comp, el, parent, props, children)
      return comp
    }
    el.render.meta = renderMeta
    io.observe(el)
  })
})
