import '@mauriceguest/app'

const ROOT_DOMAIN='profservice.moscow'

let pages='../pages'
let output = process.env['MAURICE_DEV'] ? `dev/${ROOT_DOMAIN}` : `build/${ROOT_DOMAIN}`
let WEBSITE='https://profservice.moscow'

const DIRS=['b2b','b2c','b2g']
const preprocessReplacements=[]
let cacheDir
for (const DIR of DIRS) {
  if(process.env.BUSINESS_DIRECTION==DIR) {
    pages=`../pages/${DIR}`
    cacheDir=`../pages/${DIR}/.cache`
    const DOMAIN=`${DIR}.${ROOT_DOMAIN}`
    output=process.env['MAURICE_DEV'] ? `dev/${DOMAIN}` : `build/${DOMAIN}`
    WEBSITE=`http://${DOMAIN}`

    preprocessReplacements.push({
      re: /PROFSERVICE.MOSCOW/,
      replacement() {
        // console.log(555, 'return domain')
        return DOMAIN
      },
    })

    console.log('Pages',pages)
    console.log('Output',output)
    console.log(WEBSITE)
  }
}
preprocessReplacements.push({
  re: /PROFSERVICE.MOSCOW/,
  replacement() {
    // console.log(555, 'return domain')
    return ROOT_DOMAIN
  },
})

// if(process.env.BUSINESS_DIRECTION=='b2g') {
//   pages='../pages/b2g'
//   output='../pages/b2g/docs'
//   WEBSITE='https://b2g.profservice.moscow'
// } else if(process.env.BUSINESS_DIRECTION=='b2c') {
//   pages='../pages/b2c'
//   output='../pages/b2c/docs'
//   WEBSITE='https://b2c.profservice.moscow'
// } else if(process.env.BUSINESS_DIRECTION=='b2b') {
//   pages='../pages/b2b'
//   output='../pages/b2b/docs'
//   WEBSITE='https://b2b.profservice.moscow'
// }

/** @type {guest.maurice.MauriceConfig} */
const config = {
  frontendDirs: ['types', 'maurice'],
  layout: 'maurice/layout/MainLayout.html',
  output: output,
  preprocessReplacements: preprocessReplacements,
  replacements: [
    {
      re: /{{ company }}/g,
      replacement: `[ProfService™](${WEBSITE})`,
    },
  ],
  pretty: false,
  HOST: process.env.HOST || WEBSITE,
  pages: pages,
  blocks: ['blocks'],
  elements: ['elements'],
  components: ['components'],
  // which prefixes to keep in the main CSS
  prefixes: ['-webkit-hyphens', '-ms-hyphens'],
  // for sitemap and social-buttons
  url: process.env.HOST || WEBSITE,
  // required when pages are at org.gitlab.io/pages-name
  // local: true,
  mount: '/',
  // ajax: false,
  potracePath: '~/.maurice/potrace',
  cacheDir: cacheDir,
}

export default config