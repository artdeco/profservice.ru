export function stageRight({
  voidArrow,
  children,title,stage,labelClass,ribbonClass,splendid,
  'comment-opaque':commentOpaque,
  ...props
}) {
  const img=`img/stages/top${stage}.png`

  return (<row justify-content-start PR23 {...props}>
    <StageCol commentOpaque={commentOpaque} end ribbonClass={ribbonClass} labelClass={labelClass} img={img} title={title} stage={stage}>
      {children}
    </StageCol>
    <column PR>
      {!voidArrow&&<splendid-img z-index="25" Arrow1 PA MarginAuto src="img/stages/arrow4.png" alt="arrow"  transform="scaleX(-1)"  />}
    </column>
  </row>)
}

export function stageLeft({
  children,title,stage,labelClass,ribbonClass,splendid,
  voidArrow,
  ...props
}) {
  const img=`img/stages/top${stage}.png`

  return (<row justify-content-start PR23 {...props}>
    <column PR>
      {!voidArrow&&<splendid-img z-index="25" Arrow4 PA MarginAuto src="img/stages/arrow4.png" alt="arrow"/>}
    </column>
    <StageCol start ribbonClass={ribbonClass} labelClass={labelClass} img={img} title={title} stage={stage}>
      {children}
    </StageCol>
  </row>)
}

const StageCol=({
  img, stage, title, children,ribbonClass,labelClass, end, start,
  commentOpaque,
})=>{
  return (<column lg-8 xl-8 Stage PR z-index="24">
    <splendid-img alt="decor step 1 card top" PA src={img} style="width:calc(100% + 30px)" top="0" left="-13px" right="-13px"/>
    <img Ribbon className={ribbonClass} PA src="img/stages/ribbon.png" alt="ribbon" />
    <span Label className={labelClass} PA CW FMA SB T42>
      {stage}
    </span>
    <div row justify-content-end={end?true:undefined} justify-content-start={start?true:undefined}>
      <div col col-7 text-center>
        <h3 T22 margin-top={commentOpaque?'2rem':undefined} margin-bottom={commentOpaque?'2.7rem':undefined}>
          {title}
        </h3>
        <p T14 MarginAuto width="90%">
          {children}
        </p>
      </div>
    </div>
  </column>)
}