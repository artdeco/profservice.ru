import { EllipsisSpan } from '../components/ellipsis/lib'

export default function Ellipsis({ children, splendid }) {
  splendid.pretty(false)
  splendid.export()
  const id = 'i'+splendid.random()
  return (<EllipsisSpan id={id} showing>{children}</EllipsisSpan>)
}