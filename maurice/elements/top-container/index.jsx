export default function AdvancedColumn({
  children,moto,title,title2,
}) {
  // title=`\n#* ${title}\n`

  return (
    <container z-index="10" PR>
      <row height="calc(700px - 62px)" PR>
        <column col-6 align-items-center d-flex>
          <div padding-bottom="10rem">

            <div H1Div mb-4 mt-5>
              <h1 cpf dangerouslySetInnerHTML={{__html:title}}/>
              {title2&&<h2 cpf dangerouslySetInnerHTML={{__html:title2}}/>}
            </div>

            <div MotoDiv font-weight="300">

              <p mb-5 dangerouslySetInnerHTML={{__html:moto}} />

              <p dangerouslySetInnerHTML={{__html:children[0]}}/>

            </div>
          </div>
        </column>
      </row>
    </container>)
}