export default function NumberColumn({
  children,
  'p-left':pLeft,
  'p-width':pWidth,
  'p-top':pTop,
  'n-top':nTop,
  'n-left':nLeft,
  // title,
  // iconSrc,iconAlt=title,
  // href,linkTitle,'no-justify':noJustify,
  splendid,
  noTextCenter,
  imgSrc,
  number,
  cid:cid,
  // 'description-props':descriptionProps='{}',
  ...props
}) {
  // descriptionProps=JSON.parse(descriptionProps)
  // console.log(descriptionProps)
  const descriptionProps={}
  for (const k in props) {
    if (k.startsWith('desc-')) {
      const val=props[k]
      const kk=k.replace('desc-','')
      descriptionProps[kk]=val
    }
  }
  return (
    <column ArtColumn={cid?true:undefined} {...props}>
      <img alt="декоративный фон" src={imgSrc} img-fluid  />
      <p FMA SemiBold T60 color="#22006B" PA top={nTop} left={nLeft}>
        {number}
      </p>
      <p NumberInfo T20 text-center={noTextCenter?undefined:true} PA top={pTop} left={pLeft} width={pWidth} CW
        dangerouslySetInnerHTML={{__html:children[0]}} {...descriptionProps}>
        {/* {children} */}
      </p>
    </column>)
}