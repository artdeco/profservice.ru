export default function AdvancedColumn({
  children,title,iconSrc,iconAlt=title,
  href,linkTitle,'no-justify':noJustify,
  splendid, contact,
  ...props
}) {
  let onclick
  if(contact) {
    href="javascript:void 0"
    onclick='YourName.focus'
  }
  return (<column px-4 AdvancedService text-center d-flex flex-column>
    <splendid-img above-fold align-self-center mt-2 mb-4 text-center Rotate placeholder-auto src={iconSrc} alt={iconAlt} />
    <h3 FMA T28 M mb-4>{title}</h3>

    <div {...props} Light T14 className={noJustify ? undefined : "text-justify"} mb-4 dangerouslySetInnerHTML={{__html:children[0]}}/>

    <div flex-grow-1 d-flex>
      <a onClick={onclick} align-self-end Medium href={href} B0 py-3 w-100 OrangeBtn d-block T24 p-2 BoxShadow>
        {linkTitle}
      </a>
    </div>
  </column>)
}