/**
 * @type {import('splendid').Page}
 */
export const index = {
  file: 'index',
  title: 'Клининговые решения для Вас и Вашего бизнеса',
  // seo: 'The 150-160 characters search engine meta:description',
  // og: {
  //   image: '/img/splash.jpg',
  // },
  links: {
    b2b: '//b2b.profservice.moscow',
    b2c: '//b2c.profservice.moscow',
    b2g: '//b2g.profservice.moscow',
  },
}
// /**
//  * @type {import('splendid').Page}
//  */
// export const b2c = {
//   file: 'b2c',
//   title: 'b2c',
//   seo: 'The 150-160 characters search engine meta:description',
//   og: {
//     image: '/img/splash.jpg',
//   },
//   links: {
//     gutenberg: 'https://www.gutenberg.org/files/84/84-h/84-h.htm',
//   },
// }
// /**
//  * @type {import('splendid').Page}
//  */
// export const b2g = {
//   file: 'b2g',
//   title: 'b2g',
//   // seo: 'The 150-160 characters search engine meta:description',
//   // og: {
//   //   image: '/img/splash.jpg',
//   // },
//   // links: {
//   //   gutenberg: 'https://www.gutenberg.org/files/84/84-h/84-h.htm',
//   // },
// }
// /**
//  * @type {import('splendid').Page}
//  */
// export const b2b = {
//   file: 'b2b',
//   title: 'b2b',
// }

// additional directories
// export const dir = '~/dir'
